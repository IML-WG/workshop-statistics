#!/usr/bin/env python3
import csv

# quick and dirty collecting data of user migration
# a bit redundant, but editor macros got us covered
experience = {
 'R': 0,
 'TMVA': 0,
 'keras (w/ TMVA)': 0,
 'keras (w/o TMVA)': 0,
 'neuro bayes': 0,
 'other': 0,
 'scikit-learn': 0,
 'xgboost': 0,
        }
will_stop = {
 'R': 0,
 'TMVA': 0,
 'keras (w/ TMVA)': 0,
 'keras (w/o TMVA)': 0,
 'neuro bayes': 0,
 'other': 0,
 'scikit-learn': 0,
 'xgboost': 0,
        }
will_add = {
 'R': 0,
 'TMVA': 0,
 'keras (w/ TMVA)': 0,
 'keras (w/o TMVA)': 0,
 'neuro bayes': 0,
 'other': 0,
 'scikit-learn': 0,
 'xgboost': 0,
        }
future = {
 'R': 0,
 'TMVA': 0,
 'keras (w/ TMVA)': 0,
 'keras (w/o TMVA)': 0,
 'neuro bayes': 0,
 'other': 0,
 'scikit-learn': 0,
 'xgboost': 0,
        }
attends = { }


label = {
 'R': 'R',
 'TMVA': 'TMVA',
 'keras (w/ TMVA)': 'TMVA-keras',
 'keras (w/o TMVA)': 'keras-standalone',
 'neuro bayes': 'NeuroBayes',
 'other': 'other',
 'scikit-learn': 'scikit-learn',
 'xgboost': 'XGBoost',
        }

with open('registrations.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')
    mydict = None
    for row in readCSV:
        if mydict is None:
            print(row)
            mydict = row
            print(row)
            continue

        for attend in row[mydict.index('I plan to attend in person')].split('; '):
            if attend in attends.keys():
                attends[attend] += 1
            else:
                attends[attend] = 1

        for k in future.keys():
            old = False
            new = False
            # if row[mydict.index('Primary background')] != 'CMS' : continue
            for tool in row[mydict.index('I have used in the past...')].split('; '):
                if k == tool:
                    old = True
            for tool in row[mydict.index('I plan to use in the future...')].split('; '):
                if k == tool:
                    new = True

            if old:
                experience[k] += 1
            if new:
                future[k] += 1
            if old and not new:
                will_stop[k] += 1
            if not old and new:
                will_add[k] += 1

print("People have been using:")
for k in future.keys():
    print("%s%s%d" % (k, ":\t", experience[k]))

print("\nPeople want to use:")
for k in future.keys():
    print("%s%s%d" % (k, ":\t", future[k]))

print("\nPeople want to deprecate:")
for k in future.keys():
    print("%s%s%d" % (k, ":\t", will_stop[k]))

print("\nPeople want to learn:")
for k in future.keys():
    print("%s%s%d" % (k, ":\t", will_add[k]))

with open("tool_statistics.html","w") as f:
    f.write("""<head>
  <!-- Plotly.js -->
  <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
</head>

<body>""")
    for k in future.keys():
        f.write("<div id=\"{}\"><!-- Plotly chart will be drawn inside this DIV --></div>\n".format(label[k]))
        f.write("""
  <script>
var data = {
  type: "sankey",
  orientation: "h",
  node: {
    pad: 15,
    thickness: 30,
    line: {
      color: "black",
      width: 0.5
    },
""")
        f.write("   label: [\"used {key}\", \"didn't use {key}\", \"will use {key}\", \"will not use {key}\"],\n".format(key=k))
        f.write("""color: ["blue", "blue", "blue", "blue"]
      },

  link: {
    source: [0,0,1,1],
    target: [2,3,2,3],
    """)
        f.write("value:  [{keep},{depr},{learn},{rest}]\n".format(keep=experience[k]-will_stop[k],depr=will_stop[k],learn=will_add[k],rest=286-(experience[k]+will_add[k])))
        f.write("""
      // have used - will deprecate, will deprecate, will learn, 286 - (have used - will deprecate + will learn)
  }
}

var data = [data]

var layout = {
""")
        f.write("title: \"{} user migration\",\n".format(label[k]))
        f.write("""
  font: {
    size: 10
  }
}
""")
        f.write("Plotly.react('{}', data, layout)".format(label[k]))
        f.write("""
  </script>
""")
    f.write("""
</body>
""")

print(attends)
